# Just a simple tring
simpleList = ['apple', 'orange', 'dog', 'pineapple']
print(simpleList)
print(simpleList[2])
simpleList.remove('dog')
print(simpleList)

# Just a simple dictionary 
simpleDictionary = {
    'cat': 'Meow',
    'dog': 'woof',
    'cow': 'moo'
}

animal = 'cat'
print(animal, 'goes', simpleDictionary[animal])
